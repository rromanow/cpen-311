LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.lab5_pkg.all;

ENTITY ROM IS 
   PORT ( rom_address	: in integer;
          out_note : out smallNatural);
end ROM;

ARCHITECTURE behavioral of ROM is
  type song is array ( 0 to 23 ) of smallNatural;
  constant my_Rom : song := (
    0  => 14,
    1  => 7,
    2  => 10,
    3  => 11,
    4  => 4,
    5  => 10,
    6  => 1,
    7  => 10,
    8  => 14,
    9  => 7,
    10 => 10,
    11 => 11,
    12 => 4,
    13 => 10,
    14 => 1,
    15 => 10,
    16 => 14,
    17 => 7,
    18 => 10,
    19 => 11,
	 20 => 4,
    21 => 10,
    22 => 1,
    23 => 10
	 );
begin
   process (rom_address)
   begin
     case rom_address is
       when 1 => out_note <= my_rom(0);
       when 2 => out_note <= my_rom(1);
       when 3 => out_note <= my_rom(2);
       when 4 => out_note <= my_rom(3);
       when 5 => out_note <= my_rom(4);
       when 6 => out_note <= my_rom(5);
       when 7 => out_note <= my_rom(6);
       when 8 => out_note <= my_rom(7);
       when 9 => out_note <= my_rom(8);
       when 10 => out_note <= my_rom(9);
       when 11 => out_note <= my_rom(10);
       when 12 => out_note <= my_rom(11);
       when 13 => out_note <= my_rom(12);
       when 14 => out_note <= my_rom(13);
       when 15 => out_note <= my_rom(14);
       when 16 => out_note <= my_rom(15);
		 when 17 => out_note <= my_rom(16);
       when 18 => out_note <= my_rom(17);
       when 19 => out_note <= my_rom(18);
       when 20 => out_note <= my_rom(19);
       when 21 => out_note <= my_rom(20);
       when 22 => out_note <= my_rom(21);
       when 23 => out_note <= my_rom(22);
       when 24 => out_note <= my_rom(23);
       when others => out_note <= 0;
	 end case;
  end process;
end architecture behavioral;

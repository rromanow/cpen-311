LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- This is a FIR filter, as described in the lab handout.
-- It is written as an 8-tap filter, although it can easily be changed
-- for more taps. 

ENTITY fir8 IS 
   PORT ( CLOCK_50, enable: in std_logic;
          stream_in : in std_logic_vector(23 downto 0);
          stream_out : out std_logic_vector(23 downto 0));
end fir8;

ARCHITECTURE behaviour of fir8 is
   -- Define array stucture for flip-flop memory
	type running_average is array (7 downto 0) of signed(23 downto 0);

													  
													  
	begin
	
	
	process(CLOCK_50)
	variable stream : running_average := ((others => '0'),(others => '0'),
													  (others => '0'),(others => '0'),
													  (others => '0'),(others => '0'),
													  (others => '0'),(others => '0'));	
	
	begin
	
		if( rising_edge(CLOCK_50)) then
			if (enable = '1') then
			
				--Shifting on Enable = high
				for I in 6 downto 0 loop
					stream(I+1) := stream(I);
				end loop;
				stream(0) := signed(stream_in);
				
			end if;
		end if;
		-- Addition and division by 8
		stream_out <= std_logic_vector(shift_right(stream(0),3)+shift_right(stream(1),3)
												+shift_right(stream(2),3)+shift_right(stream(3),3)
												+shift_right(stream(4),3)+shift_right(stream(5),3)
												+shift_right(stream(6),3)+shift_right(stream(7),3));
	end process;
	
end behaviour;

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.lab5_pkg.all;

ENTITY challengetask IS
	PORT (CLOCK_50,AUD_DACLRCK, AUD_ADCLRCK, AUD_BCLK,AUD_ADCDAT			:IN STD_LOGIC;
			CLOCK_27															:IN STD_LOGIC;
			KEY : IN STD_LOGIC_VECTOR (3 downto 0);
			SW																	:IN STD_LOGIC_VECTOR(17 downto 0);
        
			I2C_SDAT															:INOUT STD_LOGIC;
			I2C_SCLK,AUD_DACDAT,AUD_XCK								:OUT STD_LOGIC);
END challengetask;

ARCHITECTURE Behavior OF challengetask IS

   -- CODEC Cores
	
	COMPONENT clock_generator
		PORT(	CLOCK_27														:IN STD_LOGIC;
		    	reset															:IN STD_LOGIC;
				AUD_XCK														:OUT STD_LOGIC);
	END COMPONENT;

	COMPONENT audio_and_video_config
		PORT(	CLOCK_50,reset												:IN STD_LOGIC;
		    	I2C_SDAT														:INOUT STD_LOGIC;
				I2C_SCLK														:OUT STD_LOGIC);
	END COMPONENT;
	
	COMPONENT audio_codec
		PORT(	CLOCK_50,reset,read_s,write_s							:IN STD_LOGIC;
				writedata_left, writedata_right						:IN STD_LOGIC_VECTOR(23 DOWNTO 0);
				AUD_ADCDAT,AUD_BCLK,AUD_ADCLRCK,AUD_DACLRCK		:IN STD_LOGIC;
				read_ready, write_ready									:OUT STD_LOGIC;
				readdata_left, readdata_right							:OUT STD_LOGIC_VECTOR(23 DOWNTO 0);
				AUD_DACDAT													:OUT STD_LOGIC);
	END COMPONENT;
	
	component fir8 
   PORT ( CLOCK_50, enable: in std_logic;
          stream_in : in std_logic_vector(23 downto 0);
          stream_out : out std_logic_vector(23 downto 0));
	end component;
	
	component noise
		PORT ( CLOCK_50: in std_logic;
				magnitude : in std_logic_vector(1 downto 0);
				stream_in : in std_logic_vector(23 downto 0);
				stream_out : out std_logic_vector(23 downto 0));
	end component;
	
	component ROM 
		PORT ( rom_address	: in integer;
				 out_note : out smallNatural);
	end component;

	SIGNAL read_ready, write_ready, read_s, write_s		      :STD_LOGIC;
	SIGNAL writedata_left, writedata_right							:STD_LOGIC_VECTOR(23 DOWNTO 0);	
	SIGNAL writedata_left_pure, writedata_right_pure			:STD_LOGIC_VECTOR(23 DOWNTO 0);
	SIGNAL writedata_left_noise, writedata_right_noise			:STD_LOGIC_VECTOR(23 DOWNTO 0);
	SIGNAL writedata_left_filter, writedata_right_filter		:STD_LOGIC_VECTOR(23 DOWNTO 0);
	SIGNAL readdata_left, readdata_right							:STD_LOGIC_VECTOR(23 DOWNTO 0);	
	SIGNAL reset															:STD_LOGIC;
	SIGNAL valid															:STD_LOGIC;
	SIGNAL noise_mag,select_filter									:STD_LOGIC_VECTOR(1 downto 0);
	SIGNAL address,rom_note												:INTEGER;
	SIGNAL counter															:UNSIGNED(25 downto 0);

BEGIN

	reset <= NOT(KEY(0));
	read_s <= '0';
	noise_mag <= SW(17 downto 16);
	select_filter <= SW(15 downto 14);
	
	-- NOISE BLOCKS
	noise_left : noise PORT MAP (CLOCK_50,noise_mag,writedata_left_pure,writedata_left_noise);
	noise_right : noise PORT MAP (CLOCK_50,noise_mag,writedata_right_pure,writedata_right_noise);
	
	-- FILTER BLOCKS
	fir_right : fir8 PORT MAP (CLOCK_50, valid, writedata_right_noise,writedata_right_filter);
	fir_left  : fir8 PORT MAP (CLOCK_50, valid, writedata_left_noise,writedata_left_filter);
	
	memory : ROM port map (address,rom_note);
	
	my_clock_gen: clock_generator PORT MAP (CLOCK_27, reset, AUD_XCK);
	cfg: audio_and_video_config PORT MAP (CLOCK_50, reset, I2C_SDAT, I2C_SCLK);
	codec: audio_codec PORT MAP(CLOCK_50,
										 reset,
										 read_s,
										 write_s,
										 writedata_left, 
										 writedata_right,
										 AUD_ADCDAT,
										 AUD_BCLK,
										 AUD_ADCLRCK,
										 AUD_DACLRCK,
										 read_ready, 
										 write_ready,
										 readdata_left, 
										 readdata_right,
										 AUD_DACDAT);
										 
		-- Ignore read_ready, readdata_left, readdata_right
	
	-- ====================================================
	-- Multiplexer for signal select
	-- Toggle with SW 15 and 14
	-- ====================================================
	process(all)
		
	begin
	
		case select_filter is
			when "00" => 
				writedata_right <= writedata_right_pure;
				writedata_left  <= writedata_left_pure;
			when "10" =>
				writedata_right <= writedata_right_noise;
				writedata_left  <= writedata_left_noise;
			when "11" =>
				writedata_right <= writedata_right_filter;
				writedata_left  <= writedata_left_filter;
			when others =>
				writedata_right <= writedata_right_filter;
				writedata_left  <= writedata_left_filter;
		end case;
				
	
	end process;
		

	-- ==============================================
	-- Piano State Machine
	-- ==============================================
		  
	-- Type three process	  
	process(CLOCK_50, reset)
	
		variable state 			: play_note_state := OFF; -- State variable for state machine
		variable soundOutput 	: soundSignal;
		variable counter_root, counter_harm, counter_harm2		: smallNatural;
		variable current_note : smallNatural;
		
	begin
		--Asyncronous reset
		if(reset = '1') then
			
			
			State := OFF;
			
			
			--set the output to off
			soundOutput.r := (others => '0');
			soundOutput.l := (others => '0');
			--Reset counter_root
			counter_root := 0;
			address <= 1;
			write_s <= '0';
			valid <= '0';
			writedata_left_pure <= std_logic_vector(soundOutput.l);
			writedata_right_pure <= std_logic_vector(soundOutput.r);
			counter <= (others => '0');
		
		elsif(rising_edge(CLOCK_50)) then
			
			-- ========================================================
			-- State Machine
			-- ========================================================
			case State is 
			
				-- ========================================================
				-- OFF State
				--
				-- Self explainatory. Turn on and off with onSwitch (SW(0))
				-- ========================================================
			
				when OFF =>
				
					--Internal signals
					--set the output to off
					soundOutput.r := (others => '0');
					soundOutput.l := (others => '0');
					--Reset counter_root
					counter_root := 0;

					
					counter <= (others => '0');
					--Next state logic
					if (SW(0) = '0') then
						State := OFF;
					else
						State := WAITING_TO_WRITE;
					end if;
					
					--drive outputs
					address <= 1;
					write_s <= '0';
					valid <= '0';
					writedata_left_pure <= std_logic_vector(soundOutput.l);
					writedata_right_pure <= std_logic_vector(soundOutput.r);
					
				-- ========================================================
				-- WAITING_TO_WRITE
				--
				-- Waits till the write_ready signal goes high and then
				-- writes to the CODEC
				-- ========================================================
				
				when WAITING_TO_WRITE =>
					
					counter <= counter + 1;
					if(counter(24) = '1') then
					
						if(address = 24) then
							address <= 1;
							
						else
							address <= address + 1;
						end if;
						counter <= (others => '0');
					end if;
					current_note := rom_note;
					
					if(SW(0) = '0') then
						State := OFF;
					else
						-- Assign outputs
						valid <= '0';
						write_s <= '0';
						writedata_left_pure <= std_logic_vector(soundOutput.l);
						writedata_right_pure <= std_logic_vector(soundOutput.r);
				
						-- Square wave logic Root Frequency
						if (counter_root < note_samples_per_peak(current_note - 1) ) then
							soundOutput.r := amp.max;
							soundOutput.l := amp.max;	
						elsif (counter_root >= note_samples_per_wavelength(current_note - 1)) then
							counter_root := 0;	
						else
							soundOutput.r := amp.maxNeg;
							soundOutput.l := amp.maxNeg;
						end if;
					
						
						-- Wait for write signal to be ready
						if (write_ready = '1') then
							--Count only with the FIFO.
							counter_root := counter_root + 1;
							write_s <= '1'; 
							valid <= '1';
							State := WAITING_FOR_WRITE_READY_FALL;
						end if;
					end if;
					
				-- ========================================================
				-- WAITING_FOR_WRITE_READY_FALL
				--
				-- After writing a value to the CODEC, we need to wait till 
				-- the write_ready signal falls to zero
				-- ========================================================
					
				when WAITING_FOR_WRITE_READY_FALL =>
					-- Assign outputs
					valid <= '0';
					write_s <= '1';
					writedata_left_pure <= std_logic_vector(soundOutput.l);
					writedata_right_pure <= std_logic_vector(soundOutput.r);
					
					-- Check if write ready
					if (write_ready = '0') then
						State := WAITING_TO_WRITE;
						write_s <= '0';
					end if;
					
					-- Turn off
					if (SW(6 downto 0) = "0000000") then
						State := OFF;
					end if;
					
				-- ========================================================
				-- end state machine
				-- ========================================================
					
			end case;
						
			
		end if;	
	
	end process;
	

END Behavior;

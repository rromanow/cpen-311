library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--
-- This is a package that provides useful constants and types for Lab 5.
-- 
package lab5_pkg is
	--Where we will store our output data to the FIFO's
	type soundSignal is record 
		l : signed (23 downto 0);
		r : signed (23 downto 0);
	end record;
	
	--smaller integer for faster counters!
	subtype smallNatural is natural range 0 to 255;
	
	type note is record
		samples_per_wavelength : smallNatural;
		samples_per_peak		  : smallNatural;
--		samples_per_wavelength_harmonic : smallNatural;
--		samples_per_peak_harmonic : smallNatural;
--		samples_per_wavelength_harmonic2 : smallNatural;
--		samples_per_peak_harmonic2 : smallNatural;
	end record;
	
	type amplitude is record
		max 		: signed (23 downto 0);
		maxNeg 	: signed (23 downto 0);
--		harm		: signed (23 downto 0);
--		harmNeg  : signed (23 downto 0);
--		harm2		: signed (23 downto 0);
--		harmNeg2	: signed (23 downto 0);
	end record;
		
	type seven_smallNat is array (14 downto 0) of smallNatural;
	
	constant note_samples_per_wavelength : seven_smallNat := (183,163,145,138,122,109,97,92,82,73,69,61,55,49,65);
	constant note_samples_per_peak : seven_smallNat := (92,82,73,69,61,55,49,46,41,36,34,31,27,25,32);
			
	
		
	
	--state machine	
	type play_note_state is (OFF,WAITING_FOR_WRITE_READY_FALL,WAITING_TO_WRITE);
	

	
	constant middle_c : note := (
		samples_per_wavelength 				=> 183,
		samples_per_peak 						=> 92
--		samples_per_wavelength_harmonic 	=> 92,
--		samples_per_peak_harmonic 			=> 46,
--		samples_per_wavelength_harmonic2 => 61,
--		samples_per_peak_harmonic2			=> 30
		);
		
	constant amp : amplitude := (
		max		=> (13 => '1', others => '0'),
		maxNeg	=> to_signed(-16384,24)
		--harm		=> to_signed(6096,24),
		--harmNeg	=> to_signed(-6096,24),
		--harm2		=> to_signed(3048,24),
		--harmNeg2 => to_signed(-3048,24)
		);	
	
	CONSTANT C_SAMPLES_PER_WAVE_LENGTH : smallNatural := 183;
	CONSTANT C_SAMPLES_PER_PEAK		  : smallNatural := 92;
	
	--CONSTANT C_freq : natural := 262;
	
	--Max amplitude set to 2^16 (will change up when done)
	CONSTANT maxAmp : signed (23 downto 0) := (15 => '1', others => '0');
	CONSTANT maxAmpNeg : signed (23 downto 0) := to_signed(-65536,24);
end;

package body lab5_pkg is
end package body;
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--
-- This is a package that provides useful constants and types for Lab 5.
-- 
package lab5_pkg is
	--Where we will store our output data to the FIFO's
	type soundSignal is record 
		l : signed (23 downto 0);
		r : signed (23 downto 0);
	end record;
	
	--state machine	
	type play_note_state is (OFF,WAITING_FOR_WRITE_READY_FALL,WAITING_TO_WRITE);
	
	--smaller integer for faster counters!
	subtype smallNatural is natural range 0 to 255;
	
	CONSTANT C_SAMPLES_PER_WAVE_LENGTH : smallNatural := 183;
	CONSTANT C_SAMPLES_PER_PEAK		  : smallNatural := 92;
	
	CONSTANT C_freq : natural := 262;
	
	--Max amplitude set to 2^16 (will change up when done)
	CONSTANT maxAmp : signed (23 downto 0) := (15 => '1', others => '0');
end;

package body lab5_pkg is
end package body;
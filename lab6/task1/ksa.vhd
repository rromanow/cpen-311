library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Entity part of the description.  Describes inputs and outputs

entity ksa is
  port(CLOCK_50 : in  std_logic;  -- Clock pin
       KEY : in  std_logic_vector(3 downto 0);  -- push button switches
       SW : in  std_logic_vector(16 downto 0);  -- slider switches
		 LEDG : out std_logic_vector(7 downto 0);  -- green lights
		 LEDR : out std_logic_vector(17 downto 0));  -- red lights
end ksa;

-- Architecture part of the description

architecture rtl of ksa is

   -- Declare the component for the ram.  This should match the entity description 
	-- in the entity created by the megawizard. If you followed the instructions in the 
	-- handout exactly, it should match.  If not, look at s_memory.vhd and make the
	-- changes to the component below
	
   COMPONENT s_memory IS
	   PORT (
		   address		: IN STD_LOGIC_VECTOR (7 DOWNTO 0);
		   clock		: IN STD_LOGIC  := '1';
		   data		: IN STD_LOGIC_VECTOR (7 DOWNTO 0);
		   wren		: IN STD_LOGIC ;
		   q		: OUT STD_LOGIC_VECTOR (7 DOWNTO 0));
   END component;
	
	component result IS
	PORT(
		address		: IN STD_LOGIC_VECTOR (4 DOWNTO 0);
		clock		: IN STD_LOGIC  := '1';
		data		: IN STD_LOGIC_VECTOR (7 DOWNTO 0);
		wren		: IN STD_LOGIC ;
		q		: OUT STD_LOGIC_VECTOR (7 DOWNTO 0)
	);
	END component;
	
	component message IS
	PORT(
		address		: IN STD_LOGIC_VECTOR (4 DOWNTO 0);
		clock		: IN STD_LOGIC  := '1';
		q		: OUT STD_LOGIC_VECTOR (7 DOWNTO 0)
	);
	END component;

	-- Enumerated type for the state variable.  You will likely be adding extra
	-- state names here as you complete your design
	
	type state_type is (state_init, 
                       state_fill,
							  state_init_2,
							  state_swap,
							  state_swap_2,
							  state_swap_wait,
							  state_swap_wait_2,
							  state_swap_3,
							  state_swap_4,
							  state_swap_5,
							  state_init_3,
							  state_encrypt,
							  state_en_wait,
							  state_encrypt_2,
							  state_en_wait_2,
							  state_encrypt_3,
							  state_en_wait_3,
							  state_encrypt_4,
							  state_encrypt_5,
							  state_encrypt_6,
							  state_encrypt_7,
							  state_encrypt_final,
							  inc_sec_key,
							  BAD_DONE_STATE,
   	 					  state_done);
								
    -- These are signals that are used to connect to the memory													 
	 signal address : STD_LOGIC_VECTOR (7 DOWNTO 0);
	 signal address_m, address_r: STD_LOGIC_VECTOR (4 DOWNTO 0);	 
	 signal data,data_r : STD_LOGIC_VECTOR (7 DOWNTO 0);
	 signal wren, wren_r: STD_LOGIC;
	 signal q : STD_LOGIC_VECTOR (7 DOWNTO 0);
	 signal q_m,q_r : STD_LOGIC_VECTOR (7 DOWNTO 0);
	 --signal the_secret : STD_LOGIC_VECTOR (23 DOWNTO 0);
	 signal reset,start: STD_LOGIC;
	 constant zeros : STD_ULOGIC_VECTOR (22 DOWNTO 0) := (others => '0');
	 type secret is array (2 downto 0) of STD_LOGIC_VECTOR (7 DOWNTO 0);
	 
	 begin
	    -- Include the S memory structurally
			
       u0: s_memory port map (
	        address, CLOCK_50, data, wren, q);
		 mess : message port map (address_m, CLOCK_50, q_m);  
		 res : result port map(address_r,CLOCK_50,data_r,wren_r,q_r);
				
       -- write your code here.  As described in Slide Set 14, this 
       -- code will drive the address, data, and wren signals to
       -- fill the memory with the values 0...255
		 
		 -- Assigning hard signals
		 reset <= not KEY(0);
		 start <= SW(15);
		 
		 -- ===================================================
		 -- State Machine / Datapath
		 -- ===================================================
		 
		 process(CLOCK_50, reset)
		 
			variable i, j, k, k2: integer :=0;
			variable temp_int : integer;
			variable temp_int_2 : integer;
			variable state : state_type;
			variable temp_si : STD_LOGIC_VECTOR (7 DOWNTO 0);
			variable temp_sj, f,mess_temp : STD_LOGIC_VECTOR (7 DOWNTO 0);
			variable char : UNSIGNED(7 DOWNTO 0);
			variable the_secret : secret := ("00000000","00000000","00000000");
		 begin
		 
			if(reset = '1') then
			
				state := state_init;
				i := 0;
				
			elsif rising_edge(CLOCK_50) then
			
				 case state is
					-- ===========================================
					-- state_init
					-- ===========================================
					
					when state_init =>
						
						i := 0;
						wren <= '1';
						wren_r <= '0';
						state := state_fill;
						
					-- ===========================================
					-- state_fill
					-- ===========================================						
					
					when state_fill =>
						
						address <= std_logic_vector(to_unsigned(i,8));
						data <= std_logic_vector(to_unsigned(i,8));
						
						wren <= '1';
						i := i + 1;
						
						if i = 256 then
							state := state_init_2;
						end if;
						
					-- ===========================================
					-- state_init_2
					-- ===========================================
					
					when state_init_2 =>
					
						i := 0;
						j := 0;
						
						state := state_swap;
						
						
					-- ===========================================
					-- state_swap
					-- ===========================================
					
					when state_swap =>
						
						wren <= '0';
						address <= std_logic_vector(to_unsigned(i,8));
						state := state_swap_wait;
						
					-- ===========================================
					-- state_swap_wait
					-- ===========================================
						
					when state_swap_wait =>
						
						state := state_swap_2;
						
					-- ===========================================
					-- state_swap_2
					-- ===========================================
						
					when state_swap_2 =>
					
						temp_si := q;
						temp_int := i mod 3;
						j := (j +  to_integer(unsigned(temp_si)) + to_integer(unsigned(the_secret(temp_int)))) mod 256; 
						address <= std_logic_vector(to_unsigned(j,8));
						state := state_swap_wait_2;
						
					-- ===========================================
					-- state_swap_wait_2
					-- ===========================================
						
					when state_swap_wait_2 =>
						
						state := state_swap_3;
						
					-- ===========================================
					-- state_swap_3
					-- ===========================================
						
					when state_swap_3 =>
					
						temp_sj := q;

						state := state_swap_4;
						
					-- ===========================================
					-- state_swap_4
					-- ===========================================
					
					when state_swap_4 =>
					
						wren <= '1';
						address <= std_logic_vector(to_unsigned(i,8));
						data <= temp_sj;
						state := state_swap_5;
						
					-- ===========================================
					-- state_swap_5
					-- ===========================================
						
					when state_swap_5 =>
					
						address <= std_logic_vector(to_unsigned(j,8));
						data <= temp_si;
						state := state_swap;
						
						i := i + 1;
						
						if i = 256 then
							state := state_init_3;
						end if;
						
					-- ===========================================
					-- state_init_3
					-- This is where the decryption loop starts
					-- ===========================================
						
					when state_init_3 =>
						
						i := 0;
						j := 0;
						k := 0;
						
						if start = '1' then
							state := state_encrypt;
						end if;
						wren <= '0';
						wren_r <= '0';
						
					-- ===========================================
					-- state_encrypt
					-- ===========================================
						
					when state_encrypt =>
						
						wren <= '0';
						wren_r <= '0';
						i := (i + 1) mod 256;
						address <= std_logic_vector(to_unsigned(i,8));
						state := state_en_wait;
					
					-- ===========================================
					-- state_en_wait
					-- ===========================================
						
					when state_en_wait =>
					
						state := state_encrypt_2;
						
					-- ===========================================
					-- state_encrypt_2
					-- ===========================================
					
					when state_encrypt_2 =>
					
						temp_si := q;
						j := (j + to_integer(unsigned(temp_si))) mod 256;
						address <= std_logic_vector(to_unsigned(j,8));
						state := state_en_wait_2;
						
					-- ===========================================
					-- state_en_wait_2
					-- ===========================================
						
					when state_en_wait_2 =>
					
						state := state_encrypt_3;
						
					-- ===========================================
					-- state_encrypt_3
					-- ===========================================
					
					when state_encrypt_3 =>
						
						temp_sj := q;
						state := state_encrypt_4;
						
					-- ===========================================
					-- state_encrypt_4
					-- ===========================================
					
					when state_encrypt_4 =>
					
						address <= std_logic_vector(to_unsigned(j,8));
						wren <= '1';
						data <= temp_si;
						state := state_encrypt_5;
					
					-- ===========================================
					-- state_encrypt_5
					-- ===========================================
					
					when state_encrypt_5 =>
					
						wren <= '1';
						address <= std_logic_vector(to_unsigned(i,8));
						data <= temp_sj;
						state := state_encrypt_6;
						
					-- ===========================================
					-- state_encrypt_6
					-- ===========================================
					
					when state_encrypt_6 =>
					
						wren <= '0';
						address <= std_logic_vector(
									  to_unsigned(to_integer(unsigned(temp_si)+unsigned(temp_sj)) mod 256,8));
						address_m <= std_logic_vector(to_unsigned(k,5));
						state := state_en_wait_3;
					
					-- ===========================================
					-- state_en_wait_3
					-- ===========================================
					
					when state_en_wait_3 =>
					
						state := state_encrypt_7;
						
					-- ===========================================
					-- state_encrypt_7
					-- ===========================================
					
					when state_encrypt_7 =>
					
						wren <= '0';
						f := q;
						mess_temp := q_m;
						state := state_encrypt_final;
						
					-- ===========================================
					-- state_encrypt_final
					-- ===========================================
						
					when state_encrypt_final =>	
					
						wren_r <= '1';
						address_r <= std_logic_vector(to_unsigned(k,5));
						
						char := unsigned(f) xor unsigned(mess_temp);
						
						if ((char > to_unsigned(96,8) and char < to_unsigned(123,8)) or char = 32) then
							
							data_r <= f xor mess_temp;
							k := k + 1;
							
							if k = 32 then
								state := state_done;
							else
								state := state_encrypt;
							end if;
							
						else
							state := inc_sec_key;
						end if;
					
					-- ===========================================
					-- inc_sec_key
					-- ===========================================
									
					when inc_sec_key =>
						
						the_secret(2) := std_logic_vector(unsigned(the_secret(2)) + "00000001");
						
						if the_secret(2) = "00000000" then
						
							the_secret(1) := std_logic_vector(unsigned(the_secret(1)) + "00000001");
							
							if the_secret(1) = "00000000" then
								
								the_secret(0) := std_logic_vector(unsigned(the_secret(0)) + "00000001");
							
							end if;
						end if;
						
						if (the_secret(0)(6) = '1') then
							state := BAD_DONE_STATE;
						else
							state := state_init;
						end if;
					
					-- ===========================================
					-- BAD_DONE_STATE
					-- Enter when the secret key overflows, ie the
					-- decryption fails
					-- ===========================================
					
					when BAD_DONE_STATE =>
						LEDR <= "111111111111111111";
						LEDG <= "00000000";
						
					-- ===========================================
					-- state_done
					-- Found a coherent message
					-- ===========================================
					when state_done =>
						LEDG <= "11111111";
						LEDR <= "000000000000000000";
					when others =>
						-- Do Nothing
				 end case; 
				end if; 
			end process;
       -- You will be likely writing this is a state machine. Ensure
       -- that after the memory is filled, you enter a DONE state which
       -- does nothing but loop back to itself.  


end RTL;


